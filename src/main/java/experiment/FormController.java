package experiment;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController {

    @GetMapping("/")
    public String index() {
        return "redirect:/form";
    }

    @GetMapping("/form")
    public String formGet() {
        return "/form.html";
    }

    @PostMapping("/form")
    public String formPost(User user, @RequestParam("patient") Patient patientJson, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("patient", patientJson);
        return "form";
    }

}