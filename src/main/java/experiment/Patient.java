package experiment;

public class Patient {
    private int patientId;
    private String name;

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPatientId(){
        return this.patientId;
    }

    public String getName() {
        return this.name;
    }
}