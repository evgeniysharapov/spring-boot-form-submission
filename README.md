# Spring Boot Form Submission Conversion

This sample is to minimum viable sample of Spring Boot 2 application that shows deserialiaztion of HTML form submission parameters into POJOs.

It has a HTML form. When submitted it falls to `experiment.FormController#formPost` method and has two objects created:
1. an instance of a `User` class created from form input parameters. 
1. an instance of a `Patient` class created from de-serializing `<textarea>` content as JSON string. 

Critical to having an instance of `Patient` class de-serialized from JSON string is class `PatientConverter`. 
